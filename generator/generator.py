import os, random
import star, cross, trapezoid, circle, rectangle, regular_polygon
from general import *
from PIL import Image, ImageDraw, ImageFont, ImageEnhance

class generate:
    def __init__(self, number, rectangles, image):
        self.number = number
        self.image = image
        self.width, self.height = self.image.size

        self.selection()

        if self.shape_name == 'circle':
            points = circle.arc(self.center, 360, self.radius)
        elif self.shape_name == 'semicircle':
            points = circle.arc(self.center, 180, self.radius)
        elif self.shape_name == 'quarter_circle':
            points = circle.arc(self.center, 90, self.radius)
        elif self.shape_name == 'trapezoid':
            base = random.randint(10, 100)
            top = random.randint(10, 100)
            height = random.randint(10, 100)
            points = trapezoid.trapezoid(base, top, height, self.center[0], self.center[1], self.width, self.height)
        elif self.shape_name == 'star':
            points = star.star(self.radius, self.center[0], self.center[1])
        elif self.shape_name == 'cross':
            points = cross.cross(self.radius, self.center[0], self.center[1])
        elif self.shape_name == 'rectangle':
            rect_width = random.uniform(10, 100)
            rect_height = random.uniform(10, 100)
            points = rectangle.rectangle(rect_width, rect_height, self.center[0], self.center[1],
                                                 self.width, self.height)
        else:
            number_of_sides = side_number[self.shape_name]
            points = regular_polygon.regular_polygon(number_of_sides, self.radius, self.center[0], self.center[1])

        self.points = points

        self.rotate()
        self.compute_bounding_box()
        if 'circle' in self.shape_name:
            middle_x = ( self.max_x + self.min_x ) / 2
            middle_y = ( self.max_y + self.min_y ) / 2
            self.center = ( middle_x, middle_y )

    def compute_bounding_box(self):
        min_x = min(self.points, key=lambda tup: tup[0])[0]
        min_y = min(self.points, key=lambda tup: tup[1])[1]
        max_x = max(self.points, key=lambda tup: tup[0])[0]
        max_y = max(self.points, key=lambda tup: tup[1])[1]

        self.max_x = max_x
        self.min_x = min_x

        self.max_y = max_y
        self.min_y = min_y

        self.bounding_box = [ (min_x, min_y), (max_x, max_y),
                             (max_x, min_y), (min_x, max_y) ]

    def rotate(self):
        rotated_points = list()
        for point in self.points:
            new_point = rotate_point(point, self.tilt, self.center, self.width, self.height)
            rotated_points.append(new_point)
        self.points = rotated_points

    def selection(self):
        self.shape_name = shapes[random.randint(0, len(shapes) - 1)]
        self.shape_color = colors[random.randint(0, 4)]
        self.text_color = self.shape_color
        while self.text_color == self.shape_color:
            self.text_color = colors[random.randint(0, 4)]
        self.letter = texts[self.number]

        self.radius = random.randint(10, 100)
        self.center = ( random.randint(self.radius + 5, self.width - self.radius - 5), random.randint(self.radius + 5, self.height - self.radius - 5) )
        self.tilt = random.randint(1, 360)

def draw(image, handle, shape):
    handle.polygon(shape.points, fill=shape.shape_color, outline=shape.shape_color)

    font_file_name = f"../Fonts/{random.choice(os.listdir('../Fonts'))}"
    fnt = ImageFont.truetype(font_file_name,
                             round(((shape.max_y - shape.min_y) * 2) / 3))

    font_file_name = f"../Fonts/{random.choice(os.listdir('../Fonts'))}"
    font = ImageFont.truetype(font_file_name, round(((shape.max_y - shape.min_y) * 2) / 4))
    txt_width, txt_height = font.getsize(shape.letter)

    txt_img = Image.new('RGBA', (txt_width, txt_height), (0, 0, 0, 0))
    draw_txt = ImageDraw.Draw(txt_img)
    draw_txt.text((0, 0), text=shape.letter, font=font, fill=shape.text_color)

    txt_img = txt_img.rotate(shape.tilt, expand=1)

    px, py = (int(shape.center[0] - (shape.max_x - shape.min_x) / 4), int(shape.center[1] - (shape.max_y - shape.min_y) / 4))
    sx, sy = txt_img.size
    image.paste(txt_img, (px, py, px + sx, py + sy), txt_img)

def xml(shape, annotation):
    annotation.write("\n\t<object>\n\t\t<name>" + shape.shape_name + "</name>\n\t\t<bndbox>\n\t\t\t<xmin>"+str(shape.min_x)+"</xmin>\n\t\t\t<xmax>"+str(shape.max_x)+"</xmax>\n\t\t\t<ymin>"+str(shape.min_y)+"</ymin>\n\t\t\t<ymax>"+str(shape.max_y)+"</ymax>\n\t\t</bndbox>\n\t</object>")

def main():
    small_image = Image.open(f"../backgrounds/{random.choice(os.listdir('../backgrounds'))}")

    rectangles = list()
    shapes = list()
    number_of_shapes = random.randint(1, 10)
    width, height = small_image.size
    width *= 2
    height *= 2
    image = small_image.resize((width, height))
    draw_handle = ImageDraw.Draw(image)

    name = str(len(os.listdir(f"../Images/")) + 1) + '.png'
    annotation = open('../Annotation/' + str(len(os.listdir(f"../Annotation/")) + 1) + '.xml',"w+")
    annotation.write("<annotation>\n\t<folder>Images</folder>\n\t<filename>" + name + "</filename>\n\t<path>/Users/nikhil/Desktop/programming/UAV2020/datagenv2/Images/" + name + "</path>\n\t<size>\n\t\t<width>"+str(width)+"</width>\n\t\t<height>"+str(height)+"</height>\n\t</size>\n\t<segmented>0</segmented>")

    for number in range(number_of_shapes):
        overlap = True
        while overlap is True:
            overlap = False
            shape = generate(number, rectangles, image)
            bounding_box = shape.bounding_box
            for previous_rect in rectangles:
                if overlapping_area(previous_rect[0], previous_rect[1], bounding_box[0], bounding_box[1]) > 10:
                    overlap = True
                    break

        rectangles.append(bounding_box)
        shapes.append(shape)
        print(number)

    for shape in shapes:
        draw(image, draw_handle, shape)
        xml(shape, annotation)

    annotation.write("\n</annotation>")
    annotation.close()

    img_name = '../Images/' + str(len(os.listdir(f"../Images/")) + 1) + '.png'
    print('Filepath:', img_name)

    converter = ImageEnhance.Contrast(image)
    img2 = converter.enhance(random.randint(5,15)/20)
    converter2 = ImageEnhance.Sharpness(img2)
    img3 = converter2.enhance(random.randint(0,10)/50)

    image.save(img_name, 'png')

main()
